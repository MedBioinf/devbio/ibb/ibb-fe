import vuetifyOptions from "./utils/config/vuetify";

export default defineNuxtConfig({
  css: ["~/assets/main.css"],
  runtimeConfig: {
    public: {
      mainSpecies: "Tcas",
      apis: {
        baseUrl: "https://ibeetle-base.uni-goettingen.de/ibb/api",
        version: "v1",
        urls: {
          phenotypeservice: "",
          imageservice: "",
          ontologyservice: "",
          goannotationservice: "",
          publicationservice: "",
          querypipelineservice: "",
          geneservice: "",
        },
      },
      uiPages: {
        ontoscope: "https://ibeetle-base.uni-goettingen.de/ibb/ontoscope",
        genomebrowser: "https://ibeetle-base.uni-goettingen.de/ibb/jbrowse2/",
        apiDoc: "https://ibeetle-base.uni-goettingen.de/ibb/swagger-ui",
        blast: "https://ibeetle-base.uni-goettingen.de/ibb/blast/",
      },
    },
  },
  imports: {
    dirs: [
      "composables/repositories",
      "composables/services",
      "composables/stores",
      "composables/search",
    ],
  },
  components: [
    {
      path: "~/components",
      extensions: [".vue"],
      pathPrefix: false,
    },
  ],
  devtools: { enabled: true },
  modules: [
    "nuxt-oidc-auth",
    "vuetify-nuxt-module",
    "@pinia/nuxt",
    "@vueuse/nuxt",
    "@nuxtjs/robots",
    "@nuxtjs/sitemap",
  ],
  vuetify: vuetifyOptions,
  robots: {
    UserAgent: "*",
    Disallow: "",
  },
  sitemap: {
    include: ["/", "/help/about", "/resources"],
  },
  oidc: {
    defaultProvider: "keycloak",
    providers: {
      keycloak: {
        clientSecret: "",
        clientId: "ibb-client",
        baseUrl:
          "https://ibeetle-base.uni-goettingen.de/ibb/keycloak/realms/ibb",
        redirectUri:
          "https://ibeetle-base.uni-goettingen.de/auth/keycloak/callback",
        exposeAccessToken: true,
      },
    },
    middleware: {
      globalMiddlewareEnabled: false,
      customLoginPage: false,
    },
    session: {
      expirationCheck: true,
      automaticRefresh: true,
      expirationThreshold: 60,
    },
  },
  vite: {
    vue: {
      script: {
        defineModel: true,
      },
    },
  },
});
