FROM node:18-slim as base
WORKDIR /src

FROM base as build
COPY package.json yarn.lock ./
RUN yarn install

COPY . .
RUN yarn postinstall
RUN yarn build


FROM base

ENV HOST=0.0.0.0
ENV PORT=8080
ENV NODE_ENV=production

COPY --from=build /src/.output /src/.output

EXPOSE 8080
CMD [ "node", ".output/server/index.mjs" ]
