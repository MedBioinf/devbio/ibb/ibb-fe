import type { ApiService } from "~/utils/types/api";
import { API_SERVICES } from "~/utils/types/api";

export default defineNuxtPlugin((nuxtApp) => {
  const config = nuxtApp.$config.public.apis;
  const urls = {} as Record<ApiService, string>;
  API_SERVICES.forEach((service) => {
    urls[service] = resolveApiBaseUrl(service, config, { silent: false });
  });

  const { loggedIn, user, refresh, logout } = useOidcAuth();
  if (process.client) {
    window.onfocus = () => {
      // Logout user if token has expired
      if (loggedIn.value && user.value.expireAt * 1000 < Date.now()) {
        logout();
      }
    };
    setInterval(() => {
      // Refresh token if it will expire in 60 seconds
      if (loggedIn.value && (user.value.expireAt - 60) * 1000 < Date.now()) {
        refresh();
      }
    }, 1000 * 30);
  }

  async function apiFetch<T>(
    service: ApiService,
    request: Parameters<typeof $fetch<T>>[0],
    opts?: Parameters<typeof $fetch<T>>[1],
  ) {
    const baseURL = urls[service];
    if (loggedIn.value && user.value.accessToken) {
      const { headers, ...otherOpts } = opts || {};
      return await $fetch<T>(request, {
        ...otherOpts,
        baseURL,
        headers: {
          ...headers,
          Authorization: `Bearer ${user.value.accessToken}`,
        },
      });
    }
    return await $fetch<T>(request, { ...opts, baseURL });
  }
  return {
    provide: {
      apiFetch,
      getApiBaseUrl: (service: ApiService) => urls[service],
    },
  };
});
