import VueMatomo from "vue-matomo";

export default defineNuxtPlugin((nuxtApp) => {
  if (process.env.NODE_ENV === "production") {
    nuxtApp.vueApp.use(VueMatomo, {
      host: "https://mtb.bioinf.med.uni-goettingen.de/matomo",
      siteId: 11,
      router: nuxtApp.$router,
      enableLinkTracking: true,
      trackInitialView: true,
      disableCookies: true,
    });
  } else {
    // eslint-disable-next-line no-console
    console.log("Matomo is disabled in development mode.");
  }
});
