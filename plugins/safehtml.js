import VueSafeHTML from "vue-safe-html";

export default defineNuxtPlugin((nuxtApp) => {
  nuxtApp.vueApp.use(VueSafeHTML);
});
