import type { InjectionKey } from "vue";
import type { IBDsRNA } from "./types/ibeetle";

export const iBeetleDsRNAsKey = Symbol("iBeetle-dsRNAs") as InjectionKey<
  IBDsRNA[]
>;
export const queryPipelineKey = Symbol("query-pipeline") as InjectionKey<
  Awaited<ReturnType<typeof useQueryPipelineStepSelection>>
>;
