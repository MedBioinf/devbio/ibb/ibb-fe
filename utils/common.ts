import { promiseTimeout } from "@vueuse/core";
export const htmlGeneName = (gene: string): string => {
  // Some genes from Flybase has markups that can not be displayed directly in HTML
  return gene.replaceAll("<up>", "<sup>").replaceAll("</up>", "</sup>");
};

export const stringToURLHash = (s: string): string => {
  // Convert string to proper url hash
  return s
    .replace(/[^a-z0-9]+/gi, "-")
    .replace(/^-|-$/g, "")
    .toLowerCase();
};

/**
 * Convert a synchronous function into an asynchronous function which runs after `timeout` milliseconds
 * @param fn the input function
 * @param timeout time (ms) to wait before running the function
 * @returns a function that has the same signature as the input function
 */
export const fakeWait =
  <T extends Array<any>, U>(fn: (...args: T) => U, timeout = 1000) =>
  async (...args: T) => {
    await promiseTimeout(timeout);
    return fn(...args);
  };
