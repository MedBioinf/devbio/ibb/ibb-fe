export const createColorScale = (
  startColor: string,
  endColor: string,
  steps: number,
) => {
  const start = hexToRgb(startColor);
  const end = hexToRgb(endColor);
  const scale = [];
  for (let i = 0; i < steps; i++) {
    const r = Math.round(start.r + (end.r - start.r) * (i / steps));
    const g = Math.round(start.g + (end.g - start.g) * (i / steps));
    const b = Math.round(start.b + (end.b - start.b) * (i / steps));
    scale.push(rgbToHex(r, g, b));
  }
  return scale;
};

const hexToRgb = (hex: string) => {
  const r = parseInt(hex.slice(1, 3), 16);
  const g = parseInt(hex.slice(3, 5), 16);
  const b = parseInt(hex.slice(5, 7), 16);
  return { r, g, b };
};
const rgbToHex = (r: number, g: number, b: number) => {
  return `#${r.toString(16).padStart(2, "0")}${g.toString(16).padStart(2, "0")}${b.toString(16).padStart(2, "0")}`;
};
