export interface QueryPipelineStep {
  name: string;
  input: string;
  output: string;
  description: string;
}
