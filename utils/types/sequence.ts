export interface SequencePreview {
  text: string;
  title: string;
  sequences: {
    id: string;
    seq: string;
  }[];
}
