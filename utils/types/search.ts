export interface PartialSearchResult<T> {
  total: number;
  hits: T[];
  searchAfter: string[];
}
