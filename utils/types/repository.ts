/**
 * Repository interface
 * @param T - Type of the data fetched from the server
 * @param I - Type of the input data for user
 */
export interface Repository<T extends { id: string }, I> {
  name: string;
  data: Ref<Readonly<T[]>>;
  add: (t: I) => Promise<void>;
  remove: (id: string) => Promise<void>;
  newItem: () => I;
  preview: (input: I) => Omit<T, "id">;
  reuse?: boolean;
  reuseIgnore?: (keyof I)[];
}
