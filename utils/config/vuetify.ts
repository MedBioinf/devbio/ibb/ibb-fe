import type { ThemeDefinition } from "vuetify";
import type { ModuleOptions } from "vuetify-nuxt-module";

const lightTheme: ThemeDefinition = {
  dark: false,
  colors: {
    primary: "#6B261F",
    accent: "#FFB300",
    secondary: "#EEEEEE",
    info: "#26A69A",
    warning: "#cc9616",
    error: "#DD2C00",
    success: "#4CAF50",
  },
};

const options: ModuleOptions = {
  vuetifyOptions: {
    labComponents: true,
    directives: true,
    theme: {
      defaultTheme: "lightTheme",
      variations: {
        colors: ["primary", "secondary", "accent"],
        lighten: 2,
        darken: 2,
      },
      themes: {
        lightTheme,
      },
    },
    aliases: {
      VBtnSecondary: "VBtn",
    },
    defaults: {
      VBtnSecondary: {
        color: "grey-lighten-3",
      },
      VTextField: {
        color: "primary",
        variant: "outlined",
      },
      VTextarea: {
        color: "primary",
        variant: "outlined",
      },
      VRadio: {
        color: "primary",
      },
      VSelect: {
        color: "primary",
        variant: "outlined",
      },
      VCheckbox: {
        color: "primary",
      },
      VAutocomplete: {
        color: "primary",
        variant: "outlined",
      },
      VCombobox: {
        color: "primary",
        variant: "outlined",
      },
      VFileInput: {
        color: "primary",
        variant: "outlined",
      },
    },
  },
};

export default options;
