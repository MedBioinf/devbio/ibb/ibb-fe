import type { ApiService, ApiOptions } from "~/utils/types/api";

export const resolveApiBaseUrl = (
  service: ApiService,
  options: ApiOptions,
  { silent = true } = {},
) => {
  const { baseUrl, version } = options;
  const defaultUrl = `${baseUrl}/${service}/${version}`;
  const customUrl = options.urls?.[service];

  const clientUrl = customUrl || defaultUrl;

  if (process.client) {
    if (!silent && clientUrl !== defaultUrl) {
      // eslint-disable-next-line no-console
      console.log(`${service} custom URL: ${clientUrl}`);
    }
    return clientUrl;
  }

  if (process.server) {
    const customSSRUrl = options.ssrUrls?.[service];
    const defaultSSRUrl =
      process.env.NODE_ENV === "production"
        ? `http://${service}:8080`
        : defaultUrl;
    const serverUrl = customSSRUrl || customUrl || defaultSSRUrl;
    if (!silent && serverUrl !== defaultSSRUrl) {
      // eslint-disable-next-line no-console
      console.log(`${service} custom URL: ${serverUrl}`);
    }
    return serverUrl;
  }

  throw new Error("process is neither client nor server");
};
