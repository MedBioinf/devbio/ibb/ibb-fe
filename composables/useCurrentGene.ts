export const useCurrentGene = () => {
  const route = useRoute();
  return computed(() => {
    if (route.name !== "details-id") {
      return null;
    }
    return route.params.id.toString();
  });
};
