export const useFocusOnKeyPressed = (
  keyCombination: string,
  el: Ref<HTMLInputElement | undefined>,
) => {
  const keys = useMagicKeys();
  const keysPressed = keys[keyCombination];

  watchEffect(() => {
    if (keysPressed.value) {
      el.value?.focus();

      // In Chrome .select does not work unless wrapped in setTimeout
      // https://stackoverflow.com/a/19498477/4184843
      setTimeout(() => {
        el.value?.select();
      }, 100);
    }
  });
};
