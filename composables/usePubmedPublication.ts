import type { PublicationInput } from "~/utils/types/publication";

const fetchPubmedReference = async (pmid: string) => {
  const url = `https://api.ncbi.nlm.nih.gov/lit/ctxp/v1/pubmed/?format=citation&id=${pmid}`;
  const data = await $fetch<any>(url);
  return { reference: (data.nlm?.orig as string) || "" };
};

const fetchPubmedOtherFields = async (pmid: string) => {
  const url = `https://eutils.ncbi.nlm.nih.gov/entrez/eutils/efetch.fcgi?db=pubmed&id=${pmid}`;
  const data = await $fetch<string>(url);
  const parser = new DOMParser();
  const xmlDoc = parser.parseFromString(data || "", "text/xml");

  const journal =
    xmlDoc.querySelector("Article Journal Title")?.textContent || "";
  const authorDocs = xmlDoc.querySelectorAll("Article AuthorList Author");
  const authors = [...authorDocs].map((authorDoc) => ({
    firstName: authorDoc?.querySelector("ForeName")?.textContent || "",
    lastName: authorDoc?.querySelector("LastName")?.textContent || "",
  }));
  const title = xmlDoc.querySelector("Article ArticleTitle")?.textContent || "";
  const doi =
    xmlDoc.querySelector('Article ELocationID[EIdType="doi"')?.textContent ||
    xmlDoc.querySelector('PubmedData ArticleIdList ArticleId[IdType="doi"]')
      ?.textContent ||
    "";
  const year =
    parseInt(
      xmlDoc.querySelector("Article ArticleDate Year")?.textContent ||
        xmlDoc.querySelector("Article Journal JournalIssue PubDate Year")
          ?.textContent ||
        "",
    ) || -1;
  const abstract =
    xmlDoc.querySelector("Article Abstract AbstractText")?.textContent || "";
  return {
    doi,
    journal,
    year,
    title,
    abstract,
    authors,
  };
};

export const fetchPubmedPublication = async (pmid: string) => {
  const [part1, part2] = await Promise.all([
    fetchPubmedOtherFields(pmid),
    fetchPubmedReference(pmid),
  ]);
  return {
    ...part1,
    ...part2,
  };
};

export const usePubmedPublication = (publication: Ref<PublicationInput>) => {
  const loading = ref(0);
  const fetch = async () => {
    const pmid = publication.value.pmid;
    if (pmid === "") {
      publication.value = {
        gene: publication.value.gene,
        pmid: "",
        doi: "",
        authors: [],
        title: "",
        abstract: "",
        reference: "",
        journal: "",
        year: "",
      };
    } else {
      try {
        loading.value += 1;
        const newPublication = await fetchPubmedPublication(pmid);
        if (publication.value.pmid === pmid) {
          publication.value = {
            gene: publication.value.gene,
            ...newPublication,
            pmid,
          };
        }
      } finally {
        loading.value -= 1;
      }
    }
  };
  let timer: ReturnType<typeof setTimeout>;
  const fetchDelay = () => {
    clearTimeout(timer);
    timer = setTimeout(fetch, 400);
  };
  watch(() => publication.value.pmid, fetchDelay);
  return { loading: computed(() => loading.value !== 0) };
};
