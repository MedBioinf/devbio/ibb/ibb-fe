import type { QueryPipelineStep } from "~/utils/types/querypipeline";

export const useQueryPipelineService = () => {
  const { $apiFetch } = useNuxtApp();

  const fetchSteps = async () => {
    return (
      (await $apiFetch<QueryPipelineStep[]>(
        "querypipelineservice",
        "/steps",
      )) || []
    );
  };

  const submit = async (steps: QueryPipelineStep[], input: string) => {
    const payload = new FormData();
    payload.append("steps", JSON.stringify(steps));
    payload.append("data", input);
    return (
      (await $apiFetch<string>("querypipelineservice", "/pipelines", {
        body: payload,
        method: "POST",
      })) || ""
    );
  };

  return { fetchSteps, submit };
};
