import type { Publication, PublicationInput } from "~/utils/types/publication";

export const usePublicationService = () => {
  const { $apiFetch } = useNuxtApp();

  const fetchByGene = async (gene: string) => {
    if (!gene) return [];
    return await $apiFetch<Publication[]>(
      "publicationservice",
      `/publications/by-gene/${gene}`,
    );
  };

  const create = async (publication: PublicationInput) => {
    return await $apiFetch<Publication>("publicationservice", `/publications`, {
      method: "POST",
      body: publication,
    });
  };

  const remove = async (id: string) => {
    return await $apiFetch<Publication>(
      "publicationservice",
      `/publications/${id}`,
      {
        method: "DELETE",
      },
    );
  };

  return { fetchByGene, create, remove };
};
