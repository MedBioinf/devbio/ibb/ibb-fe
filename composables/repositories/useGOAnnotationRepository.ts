import type { Repository } from "~/utils/types/repository";
import type {
  GOAnnotation,
  GOAnnotationInput,
} from "~/utils/types/goannotation";

export const useGOAnnotationRepository = (
  gene: string,
  initialData?: GOAnnotation[],
): Repository<GOAnnotation, GOAnnotationInput> => {
  const { create, update } = useGOAnnotationService();
  const data = ref(initialData || []);
  return {
    name: "GO Annotation",
    data,
    reuse: true,
    reuseIgnore: ["term"],
    add: async (input: GOAnnotationInput) => {
      data.value.push(await create(input));
    },
    remove: async (id: string) => {
      await update([{ id, status: "TO_BE_DELETED" }]);
      data.value = data.value.filter((p) => p.id !== id);
    },
    preview: (input: GOAnnotationInput): Omit<GOAnnotation, "id"> => {
      const { pmid, ...rest } = input;
      return {
        ...rest,
        status: "UNREVIEWED",
        reference: `PMID:${pmid}`,
      };
    },
    newItem: (): GOAnnotationInput => ({
      gene,
      term: {
        id: "",
        name: "",
        aspect: "",
      },
      geneProduct: "protein",
      evidence: "",
      pmid: "",
      quotation: "",
      lab: "",
    }),
  };
};
