import type { Phenotype, PhenotypeInput } from "~/utils/types/phenotype";
import type { Repository } from "~/utils/types/repository";
import { OTHER_DSRNA } from "~/utils/types/phenotype";

export const usePhenotypeRepository = (
  gene: string,
  initialData?: Phenotype[],
): Repository<Phenotype, PhenotypeInput> => {
  const { create, remove } = usePhenotypeService();
  const data = ref(initialData || []);
  return {
    name: "Phenotype",
    data,
    add: async (phenotype: PhenotypeInput) => {
      const newPhenotype = await create(phenotype);
      data.value.push(newPhenotype);
    },
    remove: async (id: string) => {
      await remove(id);
      data.value = data.value.filter((p) => p.id !== id);
    },
    preview: (input: PhenotypeInput): Omit<Phenotype, "id"> => ({
      ...input,
      structures: [input.structure],
      images: [],
      concentration:
        input.concentration !== undefined
          ? parseFloat(input.concentration)
          : undefined,
      numberOfAnimals:
        input.numberOfAnimals !== undefined
          ? parseInt(input.numberOfAnimals)
          : undefined,
    }),
    newItem: (): PhenotypeInput => ({
      gene,
      dsRNA: {
        name: OTHER_DSRNA,
        sequence: "",
      },
      reference: {
        type: "DOI",
        value: "",
      },
      description: "",
      structure: {
        termId: "",
        termName: "",
      },
    }),
  };
};
