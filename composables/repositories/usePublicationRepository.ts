import type { Repository } from "~/utils/types/repository";
import type { Publication, PublicationInput } from "~/utils/types/publication";

export const usePublicationRepository = (
  gene: string,
  initialData?: Publication[],
): Repository<Publication, PublicationInput> => {
  const { create, remove } = usePublicationService();
  const data = ref(initialData || []);

  return {
    name: "Publication",
    data,

    add: async (publication: PublicationInput) => {
      data.value.push(await create(publication));
    },
    remove: async (id: string) => {
      await remove(id);
      data.value = data.value.filter((p) => p.id !== id);
    },
    preview: (input: PublicationInput): Omit<Publication, "id"> => input,
    newItem: (): PublicationInput => ({
      gene,
      pmid: "",
      doi: "",
      authors: [],
      title: "",
      abstract: "",
      reference: "",
      journal: "",
      year: "",
    }),
  };
};
