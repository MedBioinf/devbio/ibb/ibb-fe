import type { ImageMetadata } from "~/utils/types/image";

export const useImageRepository = (initialData?: ImageMetadata[]) => {
  const { approve, reject, imageUrl } = useImageService();
  const data = ref(initialData || []);
  return {
    data,
    approve: async (ids: string[]) => {
      await approve(ids);
      data.value = data.value.map((i) =>
        ids.includes(i.id) ? { ...i, status: "APPROVED" } : i,
      );
    },
    reject: async (ids: string[]) => {
      await reject(ids);
      data.value = data.value.filter((i) => !ids.includes(i.id));
    },
    imageUrl,
  };
};
