import type { PartialSearchResult } from "~/utils/types/search";

export const useSearchMore = <T>(
  term: Ref<string>,
  search: (
    q: string,
    searchAfter?: string[],
  ) => Promise<PartialSearchResult<T>>,
) => {
  const items = ref<T[]>([]) as Ref<T[]>;
  const loading = ref(false);
  const searchAfter = ref<string[]>();

  const refresh = async () => {
    searchAfter.value = undefined;
    if (!term.value) {
      items.value = [];
      return;
    }
    try {
      loading.value = true;
      const response = await search(term.value.trim(), searchAfter.value);
      items.value = response.hits;
      searchAfter.value = response.searchAfter;
    } finally {
      loading.value = false;
    }
  };

  const more = async () => {
    if (!term.value) {
      return;
    }
    try {
      loading.value = true;
      const response = await search(term.value.trim(), searchAfter.value);
      if (response.hits.length > 0) {
        items.value = [...items.value, ...response.hits];
        searchAfter.value = response.searchAfter;
      }
    } finally {
      loading.value = false;
    }
  };

  const debouncedRefresh = useDebounceFn(refresh, 500);
  watch(term, debouncedRefresh);
  return { items, loading, refresh, more };
};
