export interface SnackBarMessage {
  text: string;
  color: "success" | "error" | "warning";
}

export const useSnackBarStore = defineStore("snackbar", () => {
  const queue = ref<SnackBarMessage[]>([]);
  const count = computed(() => queue.value.length);
  const message = computed(() => queue.value[0]);

  const addMessage = (msg: SnackBarMessage) => {
    queue.value.push(msg);
  };

  const popMessage = () => {
    return queue.value.shift();
  };

  return { message, count, addMessage, popMessage };
});
