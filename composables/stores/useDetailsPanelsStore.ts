export const useDetailsPanelsStore = defineStore("details-panels", () => {
  const visiblePanels = ref<{ [keys: string]: boolean }>({});
  const setPanelVisibility = (key: string, visible: boolean): void => {
    if (visible) {
      visiblePanels.value[key] = true;
    } else {
      delete visiblePanels.value[key];
    }
  };

  const isPanelVisible = (key: string): boolean => {
    return !!visiblePanels.value[key];
  };

  const closedPanels = ref<{ [keys: string]: boolean }>({});
  const setPanelOpenness = (key: string, open: boolean): void => {
    if (open) {
      delete closedPanels.value[key];
    } else {
      closedPanels.value[key] = true;
    }
  };

  const isPanelOpen = (key: string): boolean => {
    return !Object.keys(closedPanels.value).includes(key);
  };

  return {
    setPanelVisibility,
    isPanelVisible,
    setPanelOpenness,
    isPanelOpen,
  };
});
