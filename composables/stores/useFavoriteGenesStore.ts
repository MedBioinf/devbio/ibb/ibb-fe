import { skipHydrate, defineStore } from "pinia";

export const useFavoriteGenesStore = defineStore("favorite-genes", () => {
  const favoriteGenes = useLocalStorage("favorite-genes", [] as string[]);
  const isFavoriteGene = (gene: string) => favoriteGenes.value.includes(gene);
  const toggleFavoriteGene = (gene: string) => {
    const idx = favoriteGenes.value.indexOf(gene);
    if (idx < 0) {
      favoriteGenes.value.unshift(gene);
    } else {
      favoriteGenes.value.splice(idx, 1);
    }
  };
  return {
    favoriteGenes: skipHydrate(favoriteGenes),
    isFavoriteGene,
    toggleFavoriteGene,
  };
});
