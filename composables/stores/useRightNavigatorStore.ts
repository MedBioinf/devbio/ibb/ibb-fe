export const useRightNavigatorStore = defineStore("right-navigator", () => {
  const { isPanelVisible } = useDetailsPanelsStore();

  const items = ref<{ title: string; hash: string }[]>([]);
  const current = computed(() => {
    return items.value.filter((item) => isPanelVisible(item.hash))[0] || null;
  });
  return { items, current };
});
