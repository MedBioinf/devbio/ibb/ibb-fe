export const useMainSpecies = () => {
  const nuxtApp = useNuxtApp();
  return nuxtApp.$config.public.mainSpecies;
};
