import type { QueryPipelineStep } from "~/utils/types/querypipeline";

export const useQueryPipelineStepSelection = (steps: QueryPipelineStep[]) => {
  const selectedSteps = ref<QueryPipelineStep[]>([]);
  const select = (step: QueryPipelineStep) => {
    selectedSteps.value.push(step);
  };

  const unselect = (step: QueryPipelineStep) => {
    const idx = selectedSteps.value.map((s) => s.name).indexOf(step.name);
    if (idx > -1) {
      selectedSteps.value.splice(idx, 1);
    }
  };

  return { steps, selectedSteps, select, unselect };
};
