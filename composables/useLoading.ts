/**
 * Create a wrapper function from the input function `fn` so that
 * `loading` is `true` when the function is about to be called and
 * `loading` is `false` when the function finishes or has errors.
 *
 * @param callback An async function
 * @returns An object with two properties:
 *  - `loading`: Indicate whether the function is running
 *  - `wrapper`: An async function with the same signature as `fn`
 */
export const useLoading = <T extends (...args: any[]) => Promise<any>>(
  fn: T,
) => {
  const loading = ref(false);
  const bindLoading =
    (fn: T) =>
    async (...args: Parameters<T>): Promise<Awaited<ReturnType<T>>> => {
      loading.value = true;
      try {
        return await fn(...args);
      } finally {
        loading.value = false;
      }
    };
  return { loading, wrapper: bindLoading(fn) };
};
