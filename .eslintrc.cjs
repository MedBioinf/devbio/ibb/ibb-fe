module.exports = {
  root: true,
  env: {
    browser: true,
    node: true,
  },
  parser: "vue-eslint-parser",
  parserOptions: {
    parser: "@typescript-eslint/parser",
  },
  extends: [
    "@nuxtjs/eslint-config-typescript",
    "plugin:vue/base",
    "plugin:vuetify/base",
    "plugin:prettier/recommended",
  ],
  plugins: ["vitest"],
  rules: {
    "vue/multi-word-component-names": "off",
    "vue/no-multiple-template-root": "off",
    "vue/v-on-event-hyphenation": "off",
    "vue/require-v-for-key": "off",
  },
};
