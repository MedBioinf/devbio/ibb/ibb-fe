# iBeetle-Base Web Front End

This project uses SSR-enabled [Nuxt 3](https://nuxt.com/docs/getting-started/introduction), which is based on [Vue.js 3](https://vuejs.org/guide/introduction.html).


## Development

### Quick start

[Node.js](https://nodejs.org/en) is required (tested on Node.js v18.19.0).

Make sure the `yarn` package manager for Node.js is installed

```bash
yarn -v
```

If not, install it

```bash
sudo npm install -g yarn
```

Go to the project directory and install the dependencies:

```bash
yarn install
```

Start the development server at the address http://localhost:3000

```bash
yarn dev
```

### Developing admin functions

The environment variable `NUXT_OIDC_PROVIDERS_KEYCLOAK_CLIENT_SECRET` needs to be set to be able to connect to Keycloak and authenticate users. The best way is to add it to `.env` file in the project directory.

### Developing with custom API services

iBeetle-Base front end depends on these API services (you can also see it in `nuxt.config.ts`):

- geneservice
- phenotypeservice
- imageservice
- ontologyservice
- goannotationservice
- publicationservice
- querypipelineservice

The default API base URL is https://ibeetle-base.uni-goettingen.de/ibb/api. For development purpose, you might want to tell the front end to use different APIs.

To change the base URL, add this to `.env` file in the project directory. If there is no such file, create it.

```
NUXT_PUBLIC_APIS_BASE_URL=https://my-custom.api/custom/path/
```

If you only want to change the API of one service, for example goannotationservice, add this:

```
NUXT_PUBLIC_APIS_URLS_GOANNOTATIONSERVICE=http://localhost:8080/
```

## Deployment

To deploy a new version, [Docker](https://www.docker.com/) need to be installed. After making changes to the code, build a new docker image (replace `1.6.1` with an appropriate tag)

```bash
docker build -t docker.gitlab.gwdg.de/medbioinf/devbio/ibb/ibb-fe:1.6.1 .
```

Push the image to gitlab. You might need permission to push

```bash
docker push docker.gitlab.gwdg.de/medbioinf/devbio/ibb/ibb-fe:1.6.1
```

Deployment server can now pull the new image to reflect the updated code.
