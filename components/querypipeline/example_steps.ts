export default [
  {
    category: "Gene Alias",
    description:
      "Convert Drosophila gene identifiers in FBgnxxxxxxx format to their corresponding symbol",
    input: "FBgn",
    name: "FBgn -> FB symbol",
    options: [],
    output: "FB symbol",
  },
  {
    category: "Orthology",
    description: "Get Tribolium orthologs for Drosophila genes using OrthoDB9",
    input: "FBgn",
    name: "FBgn -> TC",
    options: [],
    output: "TC",
  },
  {
    category: "Gene Function",
    description: "Get iB numbers for Tribolium genes",
    input: "TC",
    name: "TC -> iB",
    options: [],
    output: "iB",
  },
  {
    category: "Gene Function",
    description: "Get lethality on day 11 in iBeetle larval screen",
    input: "iB",
    name: "iB -> larval lethality day 11",
    options: [],
    output: "larval lethality day 11",
  },
  {
    category: "Gene Function",
    description: "Get lethality on day 11 in iBeetle pupal screen",
    input: "iB",
    name: "iB -> pupal lethality day 11",
    options: [],
    output: "pupal lethality day 11",
  },
];
